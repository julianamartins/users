package api.users.config;

import api.users.domain.role.Role;
import api.users.domain.role.RoleRepository;
import api.users.domain.user.User;
import api.users.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {

        User u1 = new User(null, "tricia42@dolphins.com", "Tricia", "McMillan",
                "1994-01-01", null);
        User u2 = new User(null, "tricia42_2@dolphins.com", "Tricia2", "McMillan2",
                "1994-01-01", null);

        userRepository.saveAll(Arrays.asList(u1, u2));

        Role r1 = new Role(null, "admin", null);
        Role r2 = new Role(null, "user", null);

        roleRepository.saveAll(Arrays.asList(r1, r2));
    }
}
