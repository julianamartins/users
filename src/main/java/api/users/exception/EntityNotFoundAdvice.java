package api.users.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class EntityNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<EntityNotFoundDetails> userNotFound(EntityNotFoundException exception, HttpServletRequest request) {
        String error = "Id not found";
        HttpStatus status = HttpStatus.NOT_FOUND;
        EntityNotFoundDetails entityNotFoundDetails = new EntityNotFoundDetails(status.value(), error, Instant.now(), exception.getMessage(), request.getRequestURI());
        return ResponseEntity.status(status).body(entityNotFoundDetails);
    }

    @ResponseBody
    @ExceptionHandler({DatabaseException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<EntityNotFoundDetails> database(DatabaseException exception, HttpServletRequest request) {
        String error = "Database error";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        EntityNotFoundDetails entityNotFoundDetails = new EntityNotFoundDetails(status.value(), error, Instant.now(), exception.getMessage(), request.getRequestURI());
        return ResponseEntity.status(status).body(entityNotFoundDetails);
    }
}
