package api.users.exception;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Long id) {
        super("User not found for ID: " + id);
    }
}
