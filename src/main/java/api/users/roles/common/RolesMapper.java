package api.users.roles.common;

import api.users.domain.role.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RolesMapper {

    Role toRoles(RolesDto role);
    RolesDto toRolesDto(Role role);
}
