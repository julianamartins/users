package api.users.roles.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/roles")
public class RoleCreateController {

    @Autowired
    private RoleCreateService roleCreateService;

    @PostMapping
    public RoleCreateResponse roleCreate (@Valid @RequestBody RoleCreateRequest roleCreateRequest) {
        return roleCreateService.createRole(roleCreateRequest);
    }
}
