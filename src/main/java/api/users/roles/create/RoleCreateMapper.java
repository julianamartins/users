package api.users.roles.create;

import api.users.domain.role.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleCreateMapper {

    RoleCreateResponse toRolesDto(Role role);
    Role toRoles(RoleCreateRequest role);
}
