package api.users.roles.create;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleCreateRequest {

    @NotNull
    @ApiModelProperty(example = "Admin")
    private String name;
}
