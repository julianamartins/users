package api.users.roles.create;

import lombok.Data;

@Data
public class RoleCreateResponse {

    private Long id;
}
