package api.users.roles.create;

import api.users.domain.role.Role;
import api.users.domain.role.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class RoleCreateService {

    @Autowired
    public RoleRepository roleRepository;

    @Autowired
    public RoleCreateMapper roleCreateMapper;

    public RoleCreateResponse createRole(@RequestBody RoleCreateRequest roleCreateRequest) {
        Role role = roleCreateMapper.toRoles(roleCreateRequest);
        roleRepository.save(role);
        return roleCreateMapper.toRolesDto(role);
    }
}
