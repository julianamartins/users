package api.users.roles.delete;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/roles")
public class RoleDeleteController {

    @Autowired
    private RoleDeleteService roleDeleteService;

    @DeleteMapping(value = "/{id}")
    public void roleDelete(@PathVariable Long id) {
        roleDeleteService.deleteRole(id);
    }
}
