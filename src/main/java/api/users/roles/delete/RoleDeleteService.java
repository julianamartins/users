package api.users.roles.delete;

import api.users.domain.role.RoleRepository;
import api.users.exception.DatabaseException;
import api.users.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class RoleDeleteService {

    @Autowired
    private RoleRepository roleRepository;

    public void deleteRole(Long id) {
        try {
            roleRepository.deleteById(id);
        }
        catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(id);
        }
        catch (DataIntegrityViolationException e) {
            throw new DatabaseException(e.getMessage());
        }
        return;
    }
}
