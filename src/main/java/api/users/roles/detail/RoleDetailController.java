package api.users.roles.detail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/roles")
public class RoleDetailController {

    @Autowired
    private RoleDetailService  roleDetailService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<RoleDetailResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(roleDetailService.findById(id));
    }
}
