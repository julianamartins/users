package api.users.roles.detail;

import api.users.domain.role.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleDetailMapper {

    RoleDetailResponse toRolesDto(Role role);
}
