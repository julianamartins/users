package api.users.roles.detail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleDetailRequest {

    @ApiModelProperty(example = "1")
    private Long id;
}
