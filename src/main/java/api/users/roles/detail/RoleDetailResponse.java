package api.users.roles.detail;

import lombok.Data;

@Data
public class RoleDetailResponse {

    private Long id;
    private String name;
}
