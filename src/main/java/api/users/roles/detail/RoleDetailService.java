package api.users.roles.detail;

import api.users.domain.role.Role;
import api.users.domain.role.RoleRepository;
import api.users.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleDetailService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleDetailMapper roleDetailMapper;

    public RoleDetailResponse findById(Long id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(id));
        return roleDetailMapper.toRolesDto(role);
    }
}
