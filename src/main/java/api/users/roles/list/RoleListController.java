package api.users.roles.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/roles")
public class RoleListController {

    @Autowired
    private RoleListService roleListService;

    @GetMapping
    public ResponseEntity<List<RoleListResponse>> findAll() {
        return ResponseEntity.ok().body(roleListService.findAll());
    }
}
