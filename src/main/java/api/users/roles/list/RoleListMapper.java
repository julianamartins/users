package api.users.roles.list;

import api.users.domain.role.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleListMapper {

    RoleListResponse toRolesDto(Role role);

}
