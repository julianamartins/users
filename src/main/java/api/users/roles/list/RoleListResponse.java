package api.users.roles.list;

import lombok.Data;

@Data
public class RoleListResponse {

    private Long id;
    private String name;
}
