package api.users.roles.list;

import api.users.domain.role.Role;
import api.users.domain.role.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleListService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleListMapper roleListMapper;

    public List<RoleListResponse> findAll() {
        List<Role> list = roleRepository.findAll();
        List<RoleListResponse> roleListResponse = new ArrayList<>();
        for (Role role : list) {
            roleListResponse.add(roleListMapper.toRolesDto(role));
        }
        return roleListResponse;
    }
}
