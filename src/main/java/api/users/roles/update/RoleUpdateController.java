package api.users.roles.update;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/roles")
public class RoleUpdateController {

    @Autowired
    private RoleUpdateService roleUpdateService;

    @PutMapping("/{id}")
    public RoleUpdateResponse roleUpdate(@RequestBody RoleUpdateRequest roleUpdateRequest, @PathVariable Long id) {
        return roleUpdateService.updateRole(roleUpdateRequest, id);
    }
}
