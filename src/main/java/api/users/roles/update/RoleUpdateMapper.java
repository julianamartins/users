package api.users.roles.update;

import api.users.domain.role.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleUpdateMapper {

    RoleUpdateResponse toRolesDto(Role role);
    Role toRoles(RoleUpdateRequest role);
}
