package api.users.roles.update;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleUpdateRequest {

    @ApiModelProperty(example = "1")
    private Long id;
}
