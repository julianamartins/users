package api.users.roles.update;

import lombok.Data;

@Data
public class RoleUpdateResponse {

    private Long id;
    private String name;
}
