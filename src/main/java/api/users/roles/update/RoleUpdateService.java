package api.users.roles.update;

import api.users.domain.role.Role;
import api.users.domain.role.RoleRepository;
import api.users.domain.user.User;
import api.users.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Service
public class RoleUpdateService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleUpdateMapper roleUpdateMapper;

    public RoleUpdateResponse updateRole(@RequestBody RoleUpdateRequest roleUpdateRequest, Long id) {
        try {
            Role existingRole = roleRepository.findById(id)
                                              .orElseThrow(() -> new EntityNotFoundException(id));
            updateData(existingRole, roleUpdateMapper.toRoles(roleUpdateRequest));
            roleRepository.save(existingRole);
            return roleUpdateMapper.toRolesDto(existingRole);
        }
        catch (javax.persistence.EntityNotFoundException e) {
            throw new EntityNotFoundException(id);
        }
    }

    private void updateData(Role existing, Role newUser) {
        existing.setName(newUser.getName());
    }
}
