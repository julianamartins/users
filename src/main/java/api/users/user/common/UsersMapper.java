package api.users.user.common;

import api.users.domain.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    User toUsers(UsersDto user);
    UsersDto toUsersDto(User user);
}
