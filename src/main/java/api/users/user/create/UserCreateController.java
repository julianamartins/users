package api.users.user.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserCreateController {

    @Autowired
    private UserCreateService userCreateService;

    @PostMapping
    public UserCreateResponse userCreate(@Valid @RequestBody UserCreateRequest userCreateRequest) {
        return userCreateService.createUser(userCreateRequest);
    }
}
