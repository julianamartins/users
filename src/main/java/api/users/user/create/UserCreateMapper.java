package api.users.user.create;

import api.users.domain.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserCreateMapper {

    UserCreateResponse toUsersDto(User user);
    User toUsers(UserCreateRequest user);
}
