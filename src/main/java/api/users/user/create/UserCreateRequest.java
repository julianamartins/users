package api.users.user.create;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@Data
public class UserCreateRequest {

    @NotNull
    @Email
    @ApiModelProperty(example = "name@gmail.com")
    private String email;

    @NotNull
    @ApiModelProperty(example = "Maria")
    private String firstName;

    @NotNull
    @ApiModelProperty(example = "Silva")
    private String lastName;

    @NotNull
    @DateTimeFormat
    @Past
    @ApiModelProperty(example = "yyyy-MM-dd")
    private String birthDate;

    @NotNull
    @ApiModelProperty(example = "admin")
    private String role;
}
