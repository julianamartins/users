package api.users.user.create;

import lombok.Data;

@Data
public class UserCreateResponse {

    private Long id;
}
