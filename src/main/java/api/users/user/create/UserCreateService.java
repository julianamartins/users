package api.users.user.create;

import api.users.domain.user.User;
import api.users.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class UserCreateService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCreateMapper userCreateMapper;

    public UserCreateResponse createUser(@RequestBody UserCreateRequest userCreateRequest) {
        User user = userCreateMapper.toUsers(userCreateRequest);
        userRepository.save(user);
        return userCreateMapper.toUsersDto(user);
    }
}
