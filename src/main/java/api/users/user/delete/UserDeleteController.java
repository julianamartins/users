package api.users.user.delete;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserDeleteController {

    @Autowired
    private UserDeleteService userDeleteService;

    @DeleteMapping(value = "/{id}")
    public void userDelete(@PathVariable Long id) {
        userDeleteService.deleteUser(id);
    }
}
