package api.users.user.detail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/users")
public class UserDetailController {

    @Autowired
    private UserDetailService userDetailService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDetailResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(userDetailService.findById(id));
    }
}

