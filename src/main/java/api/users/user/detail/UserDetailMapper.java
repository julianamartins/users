package api.users.user.detail;

import api.users.domain.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDetailMapper {

    UserDetailResponse toUsersDto(User user);
}
