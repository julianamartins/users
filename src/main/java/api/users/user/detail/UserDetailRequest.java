package api.users.user.detail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserDetailRequest {

    @ApiModelProperty(example = "123")
    private Long id;
}
