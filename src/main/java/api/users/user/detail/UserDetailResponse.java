package api.users.user.detail;

import lombok.Data;

@Data
public class UserDetailResponse {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String role;
}
