package api.users.user.detail;

import api.users.domain.user.User;
import api.users.domain.user.UserRepository;
import api.users.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDetailService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserDetailMapper userDetailMapper;

    public UserDetailResponse findById(Long id) {
        User user = repository.findById(id)
                              .orElseThrow(() -> new EntityNotFoundException(id));
        return userDetailMapper.toUsersDto(user);
    }
}
