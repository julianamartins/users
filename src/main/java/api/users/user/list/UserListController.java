package api.users.user.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserListController {

    @Autowired
    private UserListService userListService;

    @GetMapping
    public ResponseEntity<List<UserListResponse>> findAll() {
        return ResponseEntity.ok().body(userListService.findAll());
    }
}
