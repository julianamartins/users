package api.users.user.list;

import api.users.domain.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserListMapper {

    UserListResponse toUsersDto(User user);

}
