package api.users.user.list;

import api.users.domain.role.Role;
import lombok.Data;

@Data
public class UserListResponse {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDate;
    private Role role;
}
