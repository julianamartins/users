package api.users.user.list;

import api.users.domain.user.User;
import api.users.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserListService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserListMapper userListMapper;

    public List<UserListResponse> findAll() {
        List<User> list = repository.findAll();
        List<UserListResponse> userListResponse = new ArrayList<>();
        for (User user : list) {
            userListResponse.add(userListMapper.toUsersDto(user));
        }
        return userListResponse;
    }
}
