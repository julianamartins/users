package api.users.user.update;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users")
public class UserUpdateController {

    @Autowired
    private UserUpdateService userUpdateService;

    @PutMapping("/{id}")
    public UserUpdateResponse userUpdate(@RequestBody UserUpdateRequest userUpdateRequest, @PathVariable Long id) {
        return userUpdateService.updateUser(userUpdateRequest, id);
    }
}
