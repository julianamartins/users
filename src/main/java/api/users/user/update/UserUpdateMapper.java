package api.users.user.update;

import api.users.domain.user.User;
import api.users.user.create.UserCreateRequest;
import api.users.user.create.UserCreateResponse;
import api.users.user.list.UserListResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserUpdateMapper {

    UserUpdateResponse toUsersDto(User user);
    User toUsers(UserUpdateRequest user);
}
