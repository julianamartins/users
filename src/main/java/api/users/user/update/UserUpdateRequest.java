package api.users.user.update;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserUpdateRequest {

    @ApiModelProperty(example = "teste@email.com")
    private String email;
}
