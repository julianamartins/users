package api.users.user.update;

import lombok.Data;

@Data
public class UserUpdateResponse {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String role;
}
