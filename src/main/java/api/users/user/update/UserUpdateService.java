package api.users.user.update;

import api.users.domain.user.User;
import api.users.domain.user.UserRepository;
import api.users.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserUpdateService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserUpdateMapper userUpdateMapper;

    public UserUpdateResponse updateUser(UserUpdateRequest userUpdateRequest, Long id) {
        try {
            User existingUser = repository.findById(id)
                                          .orElseThrow(() -> new EntityNotFoundException(id));
            updateData(existingUser, userUpdateMapper.toUsers(userUpdateRequest));
            repository.save(existingUser);
            return userUpdateMapper.toUsersDto(existingUser);
        }
        catch (javax.persistence.EntityNotFoundException e) {
            throw new EntityNotFoundException(id);
        }
    }

    private void updateData(User existing, User newUser) {
        existing.setEmail(newUser.getEmail());
    }
}
