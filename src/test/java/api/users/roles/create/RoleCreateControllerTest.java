package api.users.roles.create;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RoleCreateControllerTest {

    @Test
    public void role_create_success() {
        new TestSpec()
                .given_validRequest()
                .when_service_roleCreate_then_returnValidResponse()
                .when_roleCreate()
                .then_success();
    }

    class TestSpec {
        @Mock
        RoleCreateService service;

        @InjectMocks
        RoleCreateController controller;

        RoleCreateRequest request;

        RoleCreateResponse response;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }


        public TestSpec given_validRequest() {
            request = new RoleCreateRequest();
            request.setName("admin");
            return this;
        }

        public TestSpec when_service_roleCreate_then_returnValidResponse() {
            response = new RoleCreateResponse();
            when(service.createRole(any(RoleCreateRequest.class))).thenReturn(response);
            return this;
        }

        public TestSpec when_roleCreate() {
            response = controller.roleCreate(request);
            return this;
        }

        public TestSpec then_success() {
            assertThat(response).isNotNull();
            return this;
        }
    }
}