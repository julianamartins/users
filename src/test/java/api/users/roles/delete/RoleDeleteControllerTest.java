package api.users.roles.delete;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.swing.text.html.parser.Entity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

class RoleDeleteControllerTest {

    @Test
    public void deleteRole_success() {
        new TestSpec()
                .given_validId()
                .when_service_deleteRole()
                .then_deleteRole_success();
    }

    @Test
    public void deleteRole_notExistingId() {
        new TestSpec()
                .given_invalidId()
                .when_service_deleteRole()
                .then_exception();
    }

    class TestSpec {

        @Mock
        RoleDeleteService service;

        @InjectMocks
        RoleDeleteController controller;

        Long id;

        Exception exception;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
            return this;
        }

        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(service).deleteRole(id);
            return this;
        }

        public TestSpec when_service_deleteRole() {
            try {
                controller.roleDelete(id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_deleteRole_success() {
            verify(service).deleteRole(1L);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull().isInstanceOf(EntityNotFoundException.class);
        }
    }
}