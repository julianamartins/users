package api.users.roles.detail;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

class RoleDetailControllerTest {

    @Test
    public void findById_success() {
        new TestSpec()
                .given_validId()
                .when_service_findById()
                .then_findByIdWithSuccess();
    }

    @Test
    public void findById_notFound() {
        new TestSpec()
                .given_invalidId()
                .when_service_findById()
                .then_exception();
    }

    class TestSpec {

        @Mock
        RoleDetailService service;

        @InjectMocks
        RoleDetailController controller;

        Long id;

        Exception exception;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
            return this;
        }

        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(service).findById(id);
            return this;
        }

        public TestSpec when_service_findById() {
            try {
               controller.findById(id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_findByIdWithSuccess() {
            verify(service).findById(id);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull()
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }
}