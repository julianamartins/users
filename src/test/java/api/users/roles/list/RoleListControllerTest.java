package api.users.roles.list;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

class RoleListControllerTest {

    @Test
    void findAll() {
        new TestSpec()
                .given_usersInRepository()
                .when_service_findAll()
                .then_findAllWithSuccess();
    }

    class TestSpec {
        @Mock
        RoleListService service;

        @InjectMocks
        RoleListController controller;

        RoleListResponse response;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_usersInRepository() {
            response = new RoleListResponse();
            given(service.findAll()).willReturn(Arrays.asList(response));
            return this;
        }

        public TestSpec when_service_findAll() {
            controller.findAll();
            return this;
        }

        public TestSpec then_findAllWithSuccess() {
            verify(service).findAll();
            return this;
        }
    }
}