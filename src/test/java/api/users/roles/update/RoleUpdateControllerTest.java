package api.users.roles.update;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

class RoleUpdateControllerTest {

    @Test
    void roleUpdate_success() {
        new TestSpec()
                .given_validId()
                .when_service_updateRole()
                .then_updateRole_success();
    }


    @Test
    public void userUpdate_notFound() {
        new TestSpec()
                .given_invalidId()
                .when_service_updateRole()
                .then_exception();
    }

    class TestSpec {

        @Mock
        RoleUpdateService service;

        @InjectMocks
        RoleUpdateController controller;

        RoleUpdateRequest request;

        Exception exception;

        Long id;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
            return this;
        }

        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(service).updateRole(request, id);
            return this;
        }

        public TestSpec when_service_updateRole() {
            try {
                controller.roleUpdate(request, id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_updateRole_success() {
            verify(service).updateRole(request, id);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull()
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }
}