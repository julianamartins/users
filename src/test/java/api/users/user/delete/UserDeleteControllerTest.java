package api.users.user.delete;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class UserDeleteControllerTest {

    @Test
    public void deleteUser_success() {
        new TestSpec()
                .given_validId()
                .when_service_deleteUser()
                .then_deleteUser_success();
    }

    @Test
    public void deleteUser_notExistingId() {
        new TestSpec()
                .given_invalidId()
                .when_service_deleteUser()
                .then_exception();
    }

    class TestSpec {

        @Mock
        UserDeleteService userDeleteService;

        @InjectMocks
        UserDeleteController userDeleteController;

        Long id;

        Exception exception;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
           return this;
        }


        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(userDeleteService).deleteUser(id);
            return this;
        }

        public TestSpec when_service_deleteUser() {
            try {
                userDeleteController.userDelete(id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_deleteUser_success() {
            verify(userDeleteService).deleteUser(1L);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull()
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }
}