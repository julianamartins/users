package api.users.user.detail;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

class UserDetailControllerTest {

    @Test
    public void findById_success() {
        new TestSpec()
                .given_validId()
                .when_service_findById()
                .then_findByIdWithSuccess();
    }

    @Test
    public void findById_notFound() {
        new TestSpec()
                .given_invalidId()
                .when_service_findById()
                .then_exception();
    }

    class TestSpec {

        @Mock
        UserDetailService userDetailService;

        @InjectMocks
        UserDetailController userDetailController;

        Long id;

        Exception exception;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
            return this;
        }

        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(userDetailService).findById(id);
            return this;
        }

        public TestSpec when_service_findById() {
            try {
                userDetailController.findById(id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_findByIdWithSuccess() {
            verify(userDetailService).findById(id);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull()
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }

}