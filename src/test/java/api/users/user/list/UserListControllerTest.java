package api.users.user.list;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

class UserListControllerTest {

    @Test
    public void findAll() {
        new TestSpec()
                .given_usersInRepository()
                .when_service_findAll()
                .then_findAllWithSuccess();
    }

    class TestSpec {
        @Mock
        UserListService userListService;

        @InjectMocks
        UserListController userListController;

        UserListResponse response;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_usersInRepository() {
            response = new UserListResponse();
            given(userListService.findAll()).willReturn(Arrays.asList(response));
            return this;
        }

        public TestSpec when_service_findAll() {
            userListController.findAll();
            return this;
        }

        public TestSpec then_findAllWithSuccess() {
            verify(userListService).findAll();
            return this;
        }
    }
}