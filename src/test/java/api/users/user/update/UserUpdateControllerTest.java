package api.users.user.update;

import api.users.exception.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

class UserUpdateControllerTest {

    @Test
    public void userUpdate_success() {
        new TestSpec()
                .given_validId()
                .when_service_updateUser()
                .then_updateUser_success();
    }


    @Test
    public void userUpdate_notFound() {
        new TestSpec()
                .given_invalidId()
                .when_service_updateUser()
                .then_exception();
    }

    class TestSpec {

        @Mock
        UserUpdateService userUpdateService;

        @InjectMocks
        UserUpdateController userUpdateController;

        UserUpdateRequest request;

        Exception exception;

        Long id;

        public TestSpec() {
            MockitoAnnotations.initMocks(this);
        }

        public TestSpec given_validId() {
            id = 1L;
            return this;
        }

        public TestSpec given_invalidId() {
            id = 3L;
            doThrow(new EntityNotFoundException(id)).when(userUpdateService).updateUser(request, id);
            return this;
        }

        public TestSpec when_service_updateUser() {
            try {
                userUpdateController.userUpdate(request, id);
            }
            catch (Exception e) {
                this.exception = e;
            }
            return this;
        }

        public TestSpec then_updateUser_success() {
            verify(userUpdateService).updateUser(request, id);
            return this;
        }

        public void then_exception() {
            assertThat(exception)
                    .isNotNull()
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }
}